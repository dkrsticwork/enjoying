# EnjoyingInterview

Enjoying job interview test application for candidate Dragan Krstic.  

Set **enjoying_interview.uid** in application.properties  
to adjust how the uid parameter is named in HTTP header.  
Set **enjoying_interview.dummy_uid** in application.properties.

Start with **mvn clean install spring-boot:run -DskipTests**.  
Browse to: **http://localhost:8081/bidapp/swagger-ui.html** to get to api overview.  
Browse to: **http://localhost:8081/bidapp** to get to web app.  