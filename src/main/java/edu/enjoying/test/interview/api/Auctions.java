package edu.enjoying.test.interview.api;

import edu.enjoying.test.interview.entities.Auction;
import edu.enjoying.test.interview.entities.Bid;
import edu.enjoying.test.interview.entities.Subject;
import edu.enjoying.test.interview.services.AuctionsService;
import edu.enjoying.test.interview.services.BidsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(Auctions.PATH)
public class Auctions extends EnjoyingRestApi {

    static final String PATH = EnjoyingRestApi.PATH + "/auctions";

    @Autowired
    private AuctionsService auctionsService;

    @Autowired
    private BidsService bidsService;

    @GetMapping("/")
    public Collection<Auction> readAll() {
        try {
            String uid = checkUid();
            Subject subject = subjectsService.findByUid(uid);
            return auctionsService.list();
        }
        catch (IllegalArgumentException iare) {
            throw new ResourceNotFoundException(iare.getMessage());
        }
        catch (SecurityException se) {
            throw new ForbiddenException(se.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Auction readById(@PathVariable Long id) {
        try {
            String uid = checkUid();
            Subject subject = subjectsService.findByUid(uid);
            return auctionsService.findById(id);
        }
        catch (IllegalArgumentException iare) {
            throw new ResourceNotFoundException(iare.getMessage());
        }
        catch (SecurityException se) {
            throw new ForbiddenException(se.getMessage());
        }
    }

    @PostMapping
    public Auction create(@RequestBody Auction auction) {
        try {
            String uid = checkUid();
            return auctionsService.save(auction);
        }
        catch (IllegalArgumentException iare) {
            throw new ResourceNotFoundException(iare.getMessage());
        }
        catch (SecurityException se) {
            throw new ForbiddenException(se.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Auction update(@PathVariable Long id, @RequestBody Auction auction) {
        try {
            String uid = checkUid();
            auctionsService.findById(id);
            return auctionsService.save(auction);
        }
        catch (IllegalArgumentException iare) {
            throw new ResourceNotFoundException(iare.getMessage());
        }
        catch (SecurityException se) {
            throw new ForbiddenException(se.getMessage());
        }
    }

    @GetMapping("/{id}/bids")
    public Collection<Bid> readBids(@RequestParam("id")Long id) {
        try {
            String uid = checkUid();
            Subject subject = subjectsService.findByUid(uid);
            return auctionsService.listBids(id);
        }
        catch (IllegalArgumentException iare) {
            throw new ResourceNotFoundException(iare.getMessage());
        }
        catch (SecurityException se) {
            throw new ForbiddenException(se.getMessage());
        }
    }

    @PostMapping("/{id}/bids")
    public Bid addBid(@RequestParam("id")Long id,  @RequestBody Bid bid) {
        try {
            String uid = checkUid();
            Subject subject = subjectsService.findByUid(uid);
            Auction auction = auctionsService.findById(id);
            return bidsService.addBidToAuction(bid, auction);
        }
        catch (IllegalArgumentException iare) {
            throw new ResourceNotFoundException(iare.getMessage());
        }
        catch (SecurityException se) {
            throw new ForbiddenException(se.getMessage());
        }
        catch(UnsupportedOperationException uoe) {
            throw new FailedOperationException(uoe.getMessage());
        }
    }

}
