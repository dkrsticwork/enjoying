package edu.enjoying.test.interview.api;

import edu.enjoying.test.interview.entities.Bid;
import edu.enjoying.test.interview.entities.Subject;
import edu.enjoying.test.interview.services.BidsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Bids.PATH)
public class Bids extends EnjoyingRestApi {

    static final String PATH = EnjoyingRestApi.PATH + "/bids";

    @Autowired
    private BidsService bidsService;

    @GetMapping("/{id}")
    public Bid readById(@PathVariable Long id) {
        try {
            String uid = checkUid();
            Subject subject = subjectsService.findByUid(uid);
            return bidsService.findById(id);
        }
        catch (IllegalArgumentException iare) {
            throw new EnjoyingRestApi.ResourceNotFoundException(iare.getMessage());
        }
        catch (SecurityException se) {
            throw new EnjoyingRestApi.ForbiddenException(se.getMessage());
        }
    }

}
