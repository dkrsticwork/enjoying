package edu.enjoying.test.interview.api;

import edu.enjoying.test.interview.services.SubjectsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

public abstract class EnjoyingRestApi {

    static final String PATH = "/api/v1";

    protected final Logger logr = LoggerFactory.getLogger(this.getClass());

    @Value("${enjoying_interview.uid_header}")
    private String uidHeader;

    @Autowired
    private HttpServletRequest servletRequest;

    @Autowired
    protected SubjectsService subjectsService;

    protected String checkUid() {
        String uid = servletRequest.getHeader(uidHeader);
        if(uid == null || uid.isEmpty())
            throw new SecurityException("Access denied with no uid in header.");
        else
            return uid;
    }

    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
    public static class FailedOperationException extends RuntimeException {
        public FailedOperationException(String error) {
            super(error);
        }
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class ResourceNotFoundException extends RuntimeException {
        public ResourceNotFoundException(String error) {
            super(error);
        }

    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public static class ForbiddenException extends RuntimeException {
        public ForbiddenException(String error) {
            super(error);
        }
    }

    @ExceptionHandler({ FailedOperationException.class })
    public void handleFailedOperationException() { }

    @ExceptionHandler({ ResourceNotFoundException.class })
    public void handleNotFoundException() { }

    @ExceptionHandler({ ForbiddenException.class })
    public void handleForbiddenException() { }

}
