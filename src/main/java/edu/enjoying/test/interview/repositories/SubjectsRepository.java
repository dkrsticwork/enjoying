package edu.enjoying.test.interview.repositories;

import edu.enjoying.test.interview.entities.Subject;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface SubjectsRepository extends CrudRepository<Subject, Long> {

    Collection<Subject> findByUid(String uid);

}
