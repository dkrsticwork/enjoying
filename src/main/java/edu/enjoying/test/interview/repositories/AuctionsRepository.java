package edu.enjoying.test.interview.repositories;

import edu.enjoying.test.interview.entities.Auction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuctionsRepository extends CrudRepository<Auction, Long> {
}
