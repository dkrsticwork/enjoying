package edu.enjoying.test.interview.repositories;

import edu.enjoying.test.interview.entities.Auction;
import edu.enjoying.test.interview.entities.Bid;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface BidsRepository extends CrudRepository<Bid, Long> {

    Collection<Bid> findByAuction(Auction auction);

}
