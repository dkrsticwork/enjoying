package edu.enjoying.test.interview.services;

import edu.enjoying.test.interview.entities.Subject;
import edu.enjoying.test.interview.repositories.SubjectsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;

@Service
public class SubjectsService extends AbstractEnjoyingService {

    @Value("${enjoying_interview.dummy_uid}")
    private String dummyUid = "";

    @Autowired
    private SubjectsRepository subjectsRepository;

    public Subject findByUid(String uid) {
        Collection<Subject> subjects = subjectsRepository.findByUid(uid);
        if(subjects.isEmpty())
            throw new IllegalArgumentException("Wrong uid.");
        else
            return subjects.iterator().next();
    }

    @PostConstruct
    public void init() {
        if(dummyUid !=null && !dummyUid.isEmpty()) {
            Subject dummySubject = new Subject();
            dummySubject.setUid(dummyUid);
            subjectsRepository.save(dummySubject);
        }
    }

}
