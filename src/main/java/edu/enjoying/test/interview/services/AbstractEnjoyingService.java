package edu.enjoying.test.interview.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractEnjoyingService {

    protected final Logger logr = LoggerFactory.getLogger(this.getClass());

}
