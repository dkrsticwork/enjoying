package edu.enjoying.test.interview.services;

import edu.enjoying.test.interview.entities.Auction;
import edu.enjoying.test.interview.entities.Bid;
import edu.enjoying.test.interview.repositories.AuctionsRepository;
import edu.enjoying.test.interview.repositories.BidsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class AuctionsService extends AbstractEnjoyingService {

    @Autowired
    private AuctionsRepository auctionsRepository;

    @Autowired
    private BidsRepository bidsRepository;

    public Auction save(Auction auction) {
        return auctionsRepository.save(auction);
    }

    public Collection<Auction> list() {
        List<Auction> auctions = new ArrayList<>();
        auctionsRepository.findAll().forEach(a -> auctions.add(a));
        return auctions;
    }

    public Auction findById(Long id) {
        Optional<Auction> auction = auctionsRepository.findById(id);
        if(auction.isPresent())
            return auction.get();
        else
            throw new IllegalArgumentException("Wrong auction id.");
    }

    public Collection<Bid> listBids(Long id) {
        Optional<Auction> auction = auctionsRepository.findById(id);
        if(auction.isPresent()) {
            return bidsRepository.findByAuction(auction.get());
        }
        else
            throw new IllegalArgumentException("Wrong auction id to list bids for.");
    }

}
