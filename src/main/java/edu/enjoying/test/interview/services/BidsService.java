package edu.enjoying.test.interview.services;

import edu.enjoying.test.interview.entities.Auction;
import edu.enjoying.test.interview.entities.Bid;
import edu.enjoying.test.interview.repositories.AuctionsRepository;
import edu.enjoying.test.interview.repositories.BidsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

import static edu.enjoying.test.interview.entities.Auction.STATUSES.OPEN;

@Service
public class BidsService extends AbstractEnjoyingService {

    @Autowired
    private BidsRepository bidsRepository;

    @Autowired
    private AuctionsRepository auctionsRepository;

    public Bid findById(Long id) {
        Optional<Bid> bid = bidsRepository.findById(id);
        if(bid.isPresent())
            return bid.get();
        else
            throw new IllegalArgumentException("Wrong bid id.");
    }

    public Bid addBidToAuction(Bid bid, Auction auction) {
        if(auction.getStatus() == OPEN) {
            Collection<Bid> currentBids = bidsRepository.findByAuction(auction);
            Bid highestBid = bid;
            for(Bid b : currentBids) {
                if(b.getAmount().compareTo(highestBid.getAmount()) >=  0)
                    highestBid = b;
            }
            if(highestBid != bid) {
                bid.setAuction(auction);
                return bidsRepository.save(bid);
            }
            else
                throw new UnsupportedOperationException(
                        "Auction already has bid with higher amount."
                );
        }
        else
            throw new UnsupportedOperationException(
                    "Auction closed. Cannot add bid."
            );
    }

}
