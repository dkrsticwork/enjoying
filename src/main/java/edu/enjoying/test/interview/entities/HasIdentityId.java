package edu.enjoying.test.interview.entities;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

@MappedSuperclass
public abstract class HasIdentityId {

    @Transient
    protected final Logger logr = LoggerFactory.getLogger(this.getClass());

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

}
