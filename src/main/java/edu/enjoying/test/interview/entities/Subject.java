package edu.enjoying.test.interview.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class Subject extends HasIdentityId {

    @Column
    private String uid;

}
