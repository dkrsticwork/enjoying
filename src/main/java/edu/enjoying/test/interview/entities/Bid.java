package edu.enjoying.test.interview.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Bid extends HasIdentityId {

    @Column(nullable = false)
    private Float amount;

    @ManyToOne(optional = false)
    private Subject bidder;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @ManyToOne(optional = false)
    private Auction auction;

    @PrePersist
    public void prepersist() {
        created = new Date();
    }

}
