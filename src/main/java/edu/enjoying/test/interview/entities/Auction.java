package edu.enjoying.test.interview.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Auction extends HasIdentityId {

    public enum STATUSES {
        OPEN,
        CLOSED
    }

    @Column
    private String name;

    @Column
    @Enumerated(EnumType.STRING)
    private STATUSES status = STATUSES.CLOSED;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @PrePersist
    public void prepersist() {
        created = new Date();
    }

}
